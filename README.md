
Bongo Android Code Test of Md. Shohan Ahmed
===============
- - -


1) Write a function that detects if two strings are anagram e.g. ‘bleat’ and ‘table’ are anagrams but ‘eat’ and ‘tar’ are not.

**Answer:** My answer for this question is at com.shohan.test.problem1.Anagram.java class and unit test is at com.shohan.test.problem1.AnagramUnitTest.java class.

**Solution Link:**  [Anagram](https://bitbucket.org/sasijan/shohan-test/src/master/app/src/main/java/com/shohan/test/problem1/Anagram.java)

**Unit Test Link:**  [AnagramUnitTest](https://bitbucket.org/sasijan/shohan-test/src/master/app/src/test/java/com/shohan/test/problem1/AnagramUnitTest.java)
- - -

2) Explain the design pattern used in following:

    interface Vehicle {
    int set_num_of_wheels()
    int set_num_of_passengers()
    boolean has_gas()
    }

a) Explain how you can use the pattern to create car and plane class?

**Answer:** By Factory design pattern we can use this pattern to create car and plane class. My solution is at com.shohan.test.problem2a package.

**Solution Link:**  [FactoryDesignPattern](https://bitbucket.org/sasijan/shohan-test/src/master/app/src/main/java/com/shohan/test/problem2a/DesignPattern.java)

b) Use a different design pattern for this solution.

**Answer:** I used builder design pattern to create Car & Plane class as an alternative solution. My solution is at com.shohan.test.problem2b package.

**Solution Link:**  [BuilderDesignPattern](https://bitbucket.org/sasijan/shohan-test/src/master/app/src/main/java/com/shohan/test/problem2b)
- - -

3) Write a video player application with ‘Play’, ‘Forward’, ‘Rewind’ functionalities. Please write pseudocode for this program and explain the design pattern you will use to develop these three functionalities.

**Answer:** I write an application with video ‘Play’, ‘Forward’, ‘Rewind’ functionalities. I used Kotlin, MVVM Design pattern, Exoplayer2, DataBinding etc to develop this application. It chooses a video from phone memory and play it. Please run the application from Android Studio for view the functionality and my solution is at com.shohan.test.problem3 package.

**Application Link:**  [Video Player Application](https://bitbucket.org/sasijan/shohan-test/src/master/)

**Package Link:**  [Video Player Application](https://bitbucket.org/sasijan/shohan-test/src/master/app/src/main/java/com/shohan/test/problem3)

**Play Pseudocode:**

    if VideoPlayer stage at end
       VideoPlayer seek to 0
    VideoPlayer-> PlayWhenReady

**Forward Pseudocode:**

    NextForwardTime = VideoPlayer-> CurrentTime + ForwardTimeAmount
    if NextForwardTime above of VideoTotalTime
       VideoPlayer seek to VideoTotalTime
    else
       VideoPlayer seek to NextForwardTime

**Rewind Pseudocode:**

    NextRewindTime = VideoPlayer-> CurrentTime - RewindTimeAmount
    if NextRewindTime below 0
       VideoPlayer seek to 0
    else
       VideoPlayer seek to NextRewindTime


- - -


