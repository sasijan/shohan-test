package com.shohan.test.problem1;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Local unit test of Anagram class isAnagram function
 */
public class AnagramUnitTest {
    @Test
    public void AnagramTest() {
        Anagram mAnagram = new Anagram();
        //Given test Case
        assertTrue(mAnagram.isAnagram("bleat", "table"));
        assertFalse(mAnagram.isAnagram("eat", "tar"));

        assertTrue(mAnagram.isAnagram("Shohan Ahmed", "Ahmed Shohan"));
        assertTrue(mAnagram.isAnagram("abc", "abc"));
        assertFalse(mAnagram.isAnagram("abc", "abd"));
        assertTrue(mAnagram.isAnagram("abz", "azb"));
        assertTrue(mAnagram.isAnagram("abz", "azb"));
        assertTrue(mAnagram.isAnagram("Abz", "azb"));
        assertTrue(mAnagram.isAnagram("", ""));
        assertTrue(mAnagram.isAnagram("z", "z"));
        assertTrue(mAnagram.isAnagram("z ", "z "));
        assertTrue(mAnagram.isAnagram("z 12", "z 12"));
        assertFalse(mAnagram.isAnagram("z 12", "z 13"));
    }
}