package com.shohan.test.problem1;

/**
 * Question:
 * 1) Write a function that detects if two strings are anagram e.g. ‘bleat’ and ‘table’ are
 * anagrams but ‘eat’ and ‘tar’ are not.
 */

class Anagram {

    /**
     *
     * @param firstWord First string of the anagram
     * @param secondWord Second string of the anagram
     * @return is Strings are anagram
     */
    boolean isAnagram(String firstWord, String secondWord) {

        if (firstWord.length() != secondWord.length()) {
            return false;
        }

        firstWord = firstWord.toLowerCase();
        secondWord = secondWord.toLowerCase();

        int[] charCount = new int[128]; // For Space and other character support I take full ASCII Range

        for (int i = 0; i < firstWord.length(); i++) {
            charCount[firstWord.charAt(i)]++;
            charCount[secondWord.charAt(i)]--;
        }

        //If they are Anagram then all char count must be zero.
        for (int i = 0; i < 128; i++) {
            if (charCount[i] != 0)
                return false;
        }

        return true;
    }
}
