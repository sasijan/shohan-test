package com.shohan.test.problem3.view

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util

import com.shohan.test.R
import kotlinx.android.synthetic.main.activity_video_player.*
import android.content.Intent

import android.provider.MediaStore
import android.support.v4.media.session.PlaybackStateCompat
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.exoplayer2.Player
import com.shohan.test.databinding.ActivityVideoPlayerBinding
import com.shohan.test.problem3.viewmodel.VideoPlayerViewModel


class VideoPlayerActivity : AppCompatActivity(), Player.EventListener {

    companion object {
        const val PERMISSION_CODE_STORAGE = 1001
        const val REQUEST_CODE_VIDEO = 2001
    }

    private var mVideoPlayer: SimpleExoPlayer? = null
    private lateinit var mMediaDataSourceFactory: DataSource.Factory

    private val viewModel by lazy {
        ViewModelProvider(this).get(VideoPlayerViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityVideoPlayerBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_video_player)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
        initListener()
        initObserver()
    }

    private fun initListener() {
        playerView.findViewById<View>(R.id.exo_ffwd).setOnClickListener {
            viewModel.onForwardPress()
        }
        playerView.findViewById<View>(R.id.exo_rew).setOnClickListener {
            viewModel.onRewindPress()
        }
        playerView.findViewById<View>(R.id.exo_play).setOnClickListener {
            viewModel.onPlayPress()
        }
        playerView.findViewById<View>(R.id.exo_pause).setOnClickListener {
            viewModel.onPausePress()
        }
    }


    private fun initObserver() {
        viewModel.intentForVideo.observe(this, Observer {
            if (isHasStoragePermission()) {
                chooseAVideo()
            }
        })
        viewModel.resetVideoPlayer.observe(this, Observer {
            hideStatusBar()
            initializePlayer(it.first, it.second)
        })

        viewModel.pauseVideoPlayer.observe(this, Observer {
            pausePlayer()
        })

        viewModel.playVideoPlayer.observe(this, Observer {
            playPlayer()
        })

        viewModel.seekVideoPlayer.observe(this, Observer {
            if (it.second) {
                seekForward(it.first)
            } else {
                seekBack(it.first)
            }
        })
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_CODE_STORAGE ->
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chooseAVideo()
                } else {
                    Toast.makeText(this, getString(R.string.srorage_permission_not_permit), Toast.LENGTH_LONG).show()
                }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_VIDEO) {
                val selectedImageUri = data?.data
                if (selectedImageUri != null) {
                    viewModel.onNewVideoSelected(getPath(selectedImageUri).toString())
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    public override fun onPause() {
        viewModel.onPauseActivity()
        super.onPause()
    }

    public override fun onDestroy() {
        mVideoPlayer?.stop()
        mVideoPlayer?.removeListener(this)
        mVideoPlayer?.release()
        mVideoPlayer = null
        super.onDestroy()
    }

    /**
     * Exo player state changed listener
     */
    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        viewModel.isPlaying.set((playbackState == PlaybackStateCompat.STATE_PLAYING) && playWhenReady)
    }

    /**
     * Request to choose a video
     */
    private fun chooseAVideo() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI).apply {
            type = "video/*"
            putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1)
        }
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_a_video)), REQUEST_CODE_VIDEO)
    }

    /**
     *  Is has storage permission
     */
    private fun isHasStoragePermission(): Boolean {
        val permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        val builder = AlertDialog.Builder(this, R.style.MyAlertDialogTheme)
        builder.setMessage(getString(R.string.reason_of_storage_permission))
        builder.setPositiveButton(getString(R.string.ok)) { _, _ ->
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    PERMISSION_CODE_STORAGE)
        }
        builder.setNegativeButton(getString(R.string.no)) { _, _ -> }
        builder.show()

        return false
    }

    /**
     * Init video player
     */
    private fun initializePlayer(reset: Boolean, videoPath: String) {

        if (mVideoPlayer == null) {
            mVideoPlayer = ExoPlayerFactory.newSimpleInstance(this)
            mMediaDataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "Shohan-Test"))
            mVideoPlayer?.addListener(this)
            playerView.setShutterBackgroundColor(Color.TRANSPARENT)
            playerView.player = mVideoPlayer
        }

        if (videoPath.isNotEmpty()) {
            val mediaSource = ProgressiveMediaSource.Factory(mMediaDataSourceFactory)
                    .createMediaSource(Uri.parse(videoPath))
            with(mVideoPlayer) {
                this?.prepare(mediaSource, reset, reset)
                this?.playWhenReady = false
            }
            playerView.requestFocus()
        }
    }

    /**
     * Pause video
     */
    private fun pausePlayer() {
        mVideoPlayer?.playWhenReady = false
        mVideoPlayer?.playbackState
    }

    /**
     * Play video
     */
    private fun playPlayer() {
        if (mVideoPlayer?.playbackState == Player.STATE_ENDED) {
            mVideoPlayer?.seekTo(0)
        }
        mVideoPlayer?.playWhenReady = true
    }

    /**
     * Forward video
     */
    private fun seekForward(time: Long) {
        if (mVideoPlayer?.isCurrentWindowSeekable() == true && time > 0) {
            mVideoPlayer?.getCurrentPosition()?.plus(time)?.let { finalTime ->
                val duration = mVideoPlayer?.duration
                if (duration != null && finalTime > duration)
                    mVideoPlayer?.seekTo(duration)
                else {
                    mVideoPlayer?.seekTo(finalTime)
                }
            }
        }
    }

    /**
     * Rewind video
     */
    private fun seekBack(time: Long) {
        if (mVideoPlayer?.isCurrentWindowSeekable() == true && time > 0) {
            mVideoPlayer?.getCurrentPosition()?.minus(time)?.let {
                if (it < 0)
                    mVideoPlayer?.seekTo(0)
                else
                    mVideoPlayer?.seekTo(it)
            }
        }
    }

    /**
     * Hide to status bar
     */
    private fun hideStatusBar() {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.hide()
    }

    /**
     * Get path of a video uri
     */
    private fun getPath(uri: Uri): String? {
        val projection = arrayOf(MediaStore.Video.Media.DATA)
        val cursor = contentResolver.run { query(uri, projection, null, null, null) }
        return if (cursor != null) {
            val columnIndex = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(columnIndex)
        } else {
            null
        }
    }

}