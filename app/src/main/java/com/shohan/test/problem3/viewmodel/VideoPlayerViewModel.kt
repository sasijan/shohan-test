package com.shohan.test.problem3.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class VideoPlayerViewModel : ViewModel() {


    companion object {
        const val FORWARD_TIME = 5000L
        const val REWIND_TIME = 5000L
    }

    val isPlaying = ObservableField(false)
    var intentForVideo = MutableLiveData<Boolean>()
    var resetVideoPlayer = MutableLiveData<Pair<Boolean, String>>()
    var pauseVideoPlayer = MutableLiveData<Boolean>()
    var playVideoPlayer = MutableLiveData<Boolean>()
    var seekVideoPlayer = MutableLiveData<Pair<Long, Boolean>>()

    private var mVideoFileURI = ""


    /**
     * When press on Choose Video button
     */
    fun onChooseVideoClick() {
        intentForVideo.postValue(true)
    }

    /**
     * When get new video path for play
     */
    fun onNewVideoSelected(path: String) {
        mVideoFileURI = path
        if (mVideoFileURI.isNotEmpty()) {
            resetVideoPlayer.postValue(Pair(true, mVideoFileURI))
        }
    }

    fun onPauseActivity() {
        pauseVideoPlayer.postValue(true)
    }

    /**
     * When press on pause button
     */
    fun onPausePress() {
        pauseVideoPlayer.postValue(true)
    }

    /**
     * When press on play button
     */
    fun onPlayPress() {
        playVideoPlayer.postValue(true)
    }

    /**
     * When press on forward button
     */
    fun onForwardPress() {
        seekVideoPlayer.postValue(Pair(FORWARD_TIME, true))
    }

    /**
     * When press on rewind button
     */
    fun onRewindPress() {
        seekVideoPlayer.postValue(Pair(REWIND_TIME, false))
    }
}